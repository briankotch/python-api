docker image build --build-arg "app_name=python-api" .
COMMIT=$(git rev-parse --verify HEAD)
docker image build -f "Dockerfile" . \
    --build-arg "app_name=python-api" \
    -t "python-api:latest" \
    -t "python-api:${COMMIT}"
docker image tag python-api:latest briankotch/python-api:latest
docker login
docker push briankotch/python-api:latest